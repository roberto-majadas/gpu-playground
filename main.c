#include <stdio.h>
#include <glib.h>
#include <GL/freeglut.h>

static gboolean opt_verbose;
static gint opt_win_position_x = 80;
static gint opt_win_position_y = 80;
static gint opt_win_width = 500;
static gint opt_win_height = 500;  
static gint opt_frames_per_second = 30;

gint theta;
GTimer *timer = NULL;
gint frame_count;

static GOptionEntry entries[] =
{
  { "window-x", 'x', 0, G_OPTION_ARG_INT, &opt_win_position_x, "Window position x", NULL},
  { "window-y", 'y', 0, G_OPTION_ARG_INT, &opt_win_position_y, "Window position y", NULL},
  { "window-width", 'w', 0, G_OPTION_ARG_INT, &opt_win_width, "Window width", NULL},
  { "window-height", 'h', 0, G_OPTION_ARG_INT, &opt_win_height, "Window height", NULL},
  { "fps", 'f', 0, G_OPTION_ARG_INT, &opt_frames_per_second, "Frames per second", NULL},
  { "verbose", 'v', 0, G_OPTION_ARG_NONE, &opt_verbose, "Be verbose", NULL },
  { NULL }
};

static void display_handler(void) {
  glClear(GL_COLOR_BUFFER_BIT);
  glLoadIdentity();
  glRotatef(theta, 0.0, 0.0, 1.0);
  glBegin(GL_QUADS);
  {
    glColor3f(1.0, 0.0, 0.0); glVertex2f(30, 30);
    glColor3f(0.0, 1.0, 0.0); glVertex2f(30, -30);
    glColor3f(0.0, 0.0, 1.0); glVertex2f(-30, -30);
    glColor3f(1.0, 0.0, 0.0); glVertex2f(-30, 30);
  }
  glEnd();
  glutSwapBuffers();
  
  if (timer == NULL) {
    timer = g_timer_new();
    g_timer_start(timer);
  }

  frame_count++;
  
  if (g_timer_elapsed(timer, NULL) > 1.0) {
    g_print("FPS: %i\n", frame_count);
    frame_count = 0;
    g_timer_destroy(timer);
    timer = NULL;
  }
}

static void reshape_handler(int w, int h) {
  glViewport(0, 0, w, h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(-50, 50, -50, 50);
  glMatrixMode(GL_MODELVIEW);
}

static void timeout_handler(int value) {
  glutPostRedisplay();
  glutTimerFunc(1000/opt_frames_per_second, timeout_handler, 0);
  theta++;
}

int main(int argc, char **argv) {
  GError *error = NULL;
  GOptionContext *context;

  context = g_option_context_new(NULL);
  g_option_context_add_main_entries (context, entries, NULL);
  if (!g_option_context_parse (context, &argc, &argv, &error)) {
    g_print ("option parsing failed: %s\n", error->message);
    exit (1);
  }

  glutInit(&argc, argv);

  glutInitWindowPosition(opt_win_position_x, opt_win_position_y); 
  glutInitWindowSize(opt_win_width, opt_win_height);

  glutInitDisplayMode(GLUT_RGB);

  if (glutCreateWindow("GPU Playground") == GL_FALSE) {
	  exit(1);
  }

  timer = g_timer_new();

  glutDisplayFunc(display_handler);
  glutReshapeFunc(reshape_handler);
  glutTimerFunc(1000/opt_frames_per_second, timeout_handler, 0);
  glutMainLoop();
  return 0;

}
